
<?php $this->load->view('page_header'); ?>

    <main>

        <!--Main layout-->
        <div class="container">
            <div class="row">

                <!--Sidebar-->

                <!--/.Sidebar-->

                <!--Main column-->
                <div class="col-lg-12">

                    <!--First row-->
                    <div class="row">
                        <div class="col-lg-12">

                          <nav class="navbar navbar-dark stylish-color">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">หน้าแรก</a></li>
                              <li class="breadcrumb-item active">บริจาค</li>
                            </ol>
                          </nav>
                          <br>

                              <h2 align="center" class="h2-responsive">กรองข้อมูลสิ่งของที่บริจาค</h2>


                          <!-- alert -->
                          <?php if (isset($success_msg)) { echo $success_msg; } ?>
                          <!-- end alert -->

                         <form action="<?php echo base_url(); ?>index.php/donate/donor_create" method="post" enctype="multipart/form-data">
                          <div class="row">
                            <div class="col-md-1"></div>
                                  <div class="col-md-6">
                                      <div class="md-form">
                                          <input type="text" class="form-control" name="dname" required>
                                          <label>ชื่อของที่บริจาค :</label>
                                      </div>
                                  </div>
                                  <div class="col-md-1"></div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-1"></div>
                                  <div class="col-md-5">
                                    <div class="md-form">
                                    <input type="text" class="form-control" name="dsize" required>
                                    <label>ขนาด :</label>
                                  </div>
                                  </div>
                                  <div class="col-md-5">
                                      <div class="md-form">
                                        <input type="text" class="form-control" name="dweight" required >
                                        <label>น้ำหนัก :</label>
                                      </div>
                                  </div>
                                  <div class="col-md-1"></div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-1"></div>
                                  <div class="col-md-5">
                                    <div class="md-form">
                                    <input type="text" class="form-control" name="dnum" required>
                                    <label>จำนวน(ชิ้น):</label>
                                  </div>
                                  </div>

                                  <div class="col-md-5">
                                      <div class="md-form">
                                        <input type="text" class="form-control" name="dcolor" required >
                                        <label>สี :</label>
                                      </div>
                                  </div>

                            <div class="col-md-5"></div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-1"></div>
                                  <div class="col-md-10">
                                      <div class="md-form">
                                          <textarea type="text" class="md-textarea" name="ddetail" required></textarea>
                                          <label>รายละเอียด :</label>
                                      </div>

                                  </div>
                                  <div class="col-md-1"></div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-1"></div>
                                  <div class="col-md-5">
                                    <div class="md-form">
                                    <input type="text" class="form-control" name="d่jnane" required>
                                    <label>ผู้บริจาค :</label>
                                  </div>
                                  </div>

                            <div class="col-md-1"></div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                              <!-- upload here -->
                                  <span>เลือกไฟล์</span>
                                  <input type="file" class="form-control" name="fileUpload[]" multiple>
                            </div>

                            <div class="col-md-5"> </div>
                          </div>
                          <div class="row">
                            <div class="col-md-5">
                              <div align="center" >
                              <input type="submit" value="บริจาค" class="btn btn-primary">
                              <input type="submit" value="ยกเลิก" class="btn btn-primary">

                            </div>

                          </div>


                        </form>
                      </div>
                                          </div>
                    <!--/.First row-->


                </div>
                <!--/.Main column-->

            </div>
        </div>
        <!--/.Main layout-->

    </main>

<?php $this->load->view('page_footer'); ?>
