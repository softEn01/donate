<!--Footer-->
<footer class="page-footer center-on-small-only">

    <!--Call to action-->
    <div class="call-to-action">
        <h4>ความสุขสร้างได้ ด้วยการแบ่งปัน</h4>
    </div>
    <!--/.Call to action-->

    <!--Copyright-->
    <div class="footer-copyright">
        <div class="container-fluid">
              <h2> By Ku Team </h2>
        </div>
    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->


<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-2.2.3.min.js"></script>

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/tether.min.js"></script>

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/mdb.min.js"></script>


</body>

</html>
